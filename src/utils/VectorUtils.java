package utils;

import java.util.ArrayList;

public class VectorUtils {
	public static ArrayList<Integer> not(ArrayList<Integer> bitArrayList) {

		ArrayList<Integer> tempArrayLIst = new ArrayList<Integer>();
		for (Integer i : bitArrayList) {

			switch (i) {
				case 1:
					tempArrayLIst.add(0);
					break;
				case 0:
					tempArrayLIst.add(1);
					break;
			}
		}

		return tempArrayLIst;
	}

	public static ArrayList<Integer> and(ArrayList<Integer> x1, ArrayList<Integer> x2) {
		ArrayList<Integer> temp = new ArrayList<Integer>();
		for (int i = 0; i < x1.size(); ++i) {
			temp.add(x1.get(i) & x2.get(i));
		}
		return temp;
	}

	public static Integer scalarProduct(ArrayList<Integer> x1, ArrayList<Integer> x2) {
		Integer temp = 0;
		for (int i = 0; i < x1.size(); ++i) {
			temp += (x1.get(i) & x2.get(i));
		}

		return temp % 2;
	}

	public static Integer countMajority(ArrayList<Integer> resultsOfScalarProduct) {
		int temp = 0;
		int onesCount = 0;
		int zerosCount = 0;
		for (Integer i : resultsOfScalarProduct) {
			switch (i) {
				case 1:
					++onesCount;
					break;
				case 0:
					++zerosCount;
					break;
			}
		}
		if (onesCount > zerosCount)
			temp = 1;
		else if (zerosCount > onesCount)
			temp = 0;
		else
			temp = -1;

		return temp;
	}
}
