package utils;

import java.util.ArrayList;

public class StringUtils {

	/**
	 * Generates string which is padded with zeros on the left
	 * 
	 * @param s
	 *            string before padding
	 * @param n
	 *            total length of string
	 * @return padded string
	 */
	public static String padLeft(String s, int n) {
		return String.format("%1$" + n + "s", s).replace(' ', '0');
	}

	/**
	 * Generates strings of ones which length equals 2^i
	 * 
	 * @param i
	 *            power's of 2 exponent
	 * @return string of ones
	 */
	public static String generateOnes(int i) {
		StringBuffer sb = new StringBuffer();
		long ones = (int) Math.pow(2, i);
		for (int ii = 0; ii < ones; ii++) {
			sb.append("1");
		}
		return sb.toString();
	}

	/**
	 * Generates strings of zeros which length equals 2^i
	 * 
	 * @param i
	 *            power's of 2 exponent
	 * @return string of zeros
	 */
	public static String generateZeros(int i) {
		StringBuffer sb = new StringBuffer();
		long ones = (int) Math.pow(2, i);
		for (int ii = 0; ii < ones; ii++) {
			sb.append("0");
		}
		return sb.toString();
	}

	/**
	 * Generates padded string which is binary representation of number
	 * 
	 * @param binaryNumber
	 *            number to transform
	 * @param length
	 *            total length of string
	 * @return padded string
	 */
	public static String toBinaryString(long binaryNumber, int length) {
		return padLeft(Long.toBinaryString(binaryNumber), length);
	}

	/**
	 * Generates value from binnary string
	 * 
	 * @param string
	 *            string to transform
	 * @return value
	 */
	public static long toBinaryValue(String string) {
		return Long.parseLong(string, 2);
	}
	
	public static ArrayList<Integer> stringToArray(String string){
		ArrayList<Integer> list = new ArrayList<Integer>();
		for(int i=0; i<string.length(); i++){
			list.add(Character.getNumericValue(string.charAt(i)));
		}
		return list;
	}

}
