package decoding;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import main.RMFrame;

import utils.StringUtils;
import utils.VectorUtils;

import encoding.ExtendedGenerationMatrix;

public class Decoder {

	private String correctMessageString;
	/**
	 * My vector
	 */
	private String MyVector = "";

	/**
	 * Generation matrix prepared for decoding
	 */
	private ExtendedGenerationMatrix matrix;
	/**
	 * Bits of correct message
	 */
	private ArrayList<Integer> correctMessage;
	/**
	 * HashMap translating vector's number (symbol) from
	 * ExtendedGenarationMatrix into majority score
	 */
	private HashMap<Set<Integer>, Integer> vectorNumberToMajorityMap;
	private int charactristicVectorsNumber;

	public Decoder() {
		correctMessage = new ArrayList<Integer>();
		vectorNumberToMajorityMap = new HashMap<Set<Integer>, Integer>();
		this.matrix = new ExtendedGenerationMatrix(RMFrame.rValue, RMFrame.mValue);
	}

	public String getDecodedMessage() {
		return correctMessageString;
	}

	public int decodeMessage(String message) {

		int messageCorrectLength = (int) Math.pow(2, RMFrame.mValue);

		ArrayList<Integer> encodedMessage = new ArrayList<Integer>();
		for (int i = 0; i < messageCorrectLength; ++i) {
			encodedMessage.add((int) (message.charAt(i)) - 48);
		}
		System.out.println(encodedMessage);
		matrix = new ExtendedGenerationMatrix(RMFrame.rValue, RMFrame.mValue);

		charactristicVectorsNumber = matrix.getCharacteristicVectorsNumber();
		Map<Set<Integer>, Set<Integer>> characteristicVectors = matrix.getCharacteristicVectors();
		Map<Set<Integer>, ArrayList<ArrayList<Integer>>> mapOfCharacteristicVectorsListToMajorityCount = createMapOfCharacteristicVectorsListToMajorityCount(characteristicVectors);

		for (Map.Entry<Set<Integer>, ArrayList<ArrayList<Integer>>> entry : mapOfCharacteristicVectorsListToMajorityCount
				.entrySet()) {
			if (decode(encodedMessage, entry.getValue(), entry.getKey()) == -1) {
				return -1;
			}
		}

		MyVector = countMyVector();

		if (MyVector.equals("")) {
			MyVector = StringUtils.generateZeros(RMFrame.mValue);
		}

		finalCountdown(MyVector, message);
		correctMessageString = getOriginalMessage(correctMessage);
		System.out.println("ORIGINAL MESSAGE: " + correctMessageString);

		return 1;
	}

	private Map<Set<Integer>, ArrayList<ArrayList<Integer>>> createMapOfCharacteristicVectorsListToMajorityCount(
			Map<Set<Integer>, Set<Integer>> characteristicVectors) {
		Map<Set<Integer>, ArrayList<ArrayList<Integer>>> mapOfCharacteristicVectorsListToMajorityCount = new LinkedHashMap<Set<Integer>, ArrayList<ArrayList<Integer>>>();
		for (Map.Entry<Set<Integer>, Set<Integer>> entry : characteristicVectors.entrySet()) {
			ArrayList<ArrayList<Integer>> characteristicVectorsList = new ArrayList<ArrayList<Integer>>();

			if (entry.getValue().size() == 1) {
				String baseVector = matrix.getMatrix().get(entry.getValue());
				ArrayList<Integer> vector = StringUtils.stringToArray(baseVector);
				ArrayList<Integer> notVector = VectorUtils.not(vector);
				characteristicVectorsList.add(vector);
				characteristicVectorsList.add(notVector);

			}

			if (entry.getValue().size() == 2) {
				ArrayList<ArrayList<Integer>> baseVectors = new ArrayList<ArrayList<Integer>>();

				for (Integer vectorKey : entry.getValue()) {
					String baseVector = matrix.getMatrix().get(new HashSet<Integer>(Arrays.asList(vectorKey)));
					ArrayList<Integer> vector = StringUtils.stringToArray(baseVector);
					baseVectors.add(vector);
				}

				ArrayList<Integer> x1 = baseVectors.get(0);
				ArrayList<Integer> x2 = baseVectors.get(1);
				ArrayList<Integer> notX1 = VectorUtils.not(x1);
				ArrayList<Integer> notX2 = VectorUtils.not(x2);

				if (charactristicVectorsNumber == 2) {
					characteristicVectorsList.add(VectorUtils.and(notX2, notX1));
					characteristicVectorsList.add(VectorUtils.not(VectorUtils.and(notX1, notX2)));

				} else {
					characteristicVectorsList.add(VectorUtils.and(x1, x2));
					characteristicVectorsList.add(VectorUtils.and(x1, notX2));
					characteristicVectorsList.add(VectorUtils.and(notX1, x2));
					characteristicVectorsList.add(VectorUtils.and(notX1, notX2));

				}

			}

			if (entry.getValue().size() == 3) {
				ArrayList<ArrayList<Integer>> baseVectors = new ArrayList<ArrayList<Integer>>();

				for (Integer vectorKey : entry.getValue()) {
					String baseVector = matrix.getMatrix().get(new HashSet<Integer>(Arrays.asList(vectorKey)));
					ArrayList<Integer> vector = StringUtils.stringToArray(baseVector);
					baseVectors.add(vector);
				}

				ArrayList<Integer> x1 = baseVectors.get(0);
				ArrayList<Integer> x2 = baseVectors.get(1);
				ArrayList<Integer> x3 = baseVectors.get(2);
				ArrayList<Integer> notX1 = VectorUtils.not(x1);
				ArrayList<Integer> notX2 = VectorUtils.not(x2);
				ArrayList<Integer> notX3 = VectorUtils.not(x3);

				if (charactristicVectorsNumber == 4) {

					// ???

				} else {

					characteristicVectorsList.add(VectorUtils.and(VectorUtils.and(x1, x2), x3));
					characteristicVectorsList.add(VectorUtils.and(VectorUtils.and(x1, x2), notX3));
					characteristicVectorsList.add(VectorUtils.and(VectorUtils.and(x1, notX2), x3));
					characteristicVectorsList.add(VectorUtils.and(VectorUtils.and(x1, notX2), notX3));
					characteristicVectorsList.add(VectorUtils.and(VectorUtils.and(notX1, x2), x3));
					characteristicVectorsList.add(VectorUtils.and(VectorUtils.and(notX1, x2), notX3));
					characteristicVectorsList.add(VectorUtils.and(VectorUtils.and(notX1, notX2), x3));
					characteristicVectorsList.add(VectorUtils.and(VectorUtils.and(notX1, notX2), notX3));
				}
			}

			mapOfCharacteristicVectorsListToMajorityCount.put(entry.getKey(), characteristicVectorsList);
		}
		return mapOfCharacteristicVectorsListToMajorityCount;
	}

	private int decode(ArrayList<Integer> encodedMessage, ArrayList<ArrayList<Integer>> characteristicVectorsList,
			Set<Integer> row) {
		ArrayList<Integer> resultsOfScalarProduct = new ArrayList<Integer>();
		int majority = -7;

		for (int i = 0; i < characteristicVectorsList.size(); ++i) {
			resultsOfScalarProduct.add(VectorUtils.scalarProduct(characteristicVectorsList.get(i), encodedMessage));
		}
		majority = VectorUtils.countMajority(resultsOfScalarProduct);
		System.out.println("Majority is " + majority);
		vectorNumberToMajorityMap.put(row, majority);
		correctMessage.add(majority);

		return majority;
	}

	private String countMyVector() {
		String MyVector = "";

		for (Map.Entry<Set<Integer>, Integer> entry : vectorNumberToMajorityMap.entrySet()) {

			if (entry.getValue() == 1) {

				if (MyVector.equals("")) {
					MyVector = matrix.getMatrix().get(new HashSet<Integer>(entry.getKey()));
				} else {
					long longMy = Long.parseLong(MyVector, 2);
					long v = Long.parseLong(matrix.getMatrix().get(new HashSet<Integer>(entry.getKey())), 2);
					longMy ^= v;
					MyVector = StringUtils.toBinaryString(longMy, (int) (Math.pow(2, RMFrame.mValue)));
				}
			}
		}

		return MyVector;
	}

	private void finalCountdown(String my, String me) {

		System.out.println("MY: " + my);
		System.out.println("ME: " + me);
		
		long result = Long.parseLong(my, 2);
		long longMe = Long.parseLong(me, 2);
		result ^= longMe;
		int majority = -7;
		ArrayList<Integer> bitList = new ArrayList<Integer>();
		for (char c : StringUtils.toBinaryString(result, (int) (Math.pow(2, RMFrame.mValue))).toCharArray()) {
			bitList.add((int) c - 48);
		}

		majority = VectorUtils.countMajority(bitList);
		System.out.println("MAJORITY PIERWSZEGO WIERSZA: " + majority);
		correctMessage.add(majority);
	}

	private String getOriginalMessage(ArrayList<Integer> list) {
		String msg = "";
		StringBuilder sb = new StringBuilder();
		sb.append(String.valueOf(list.get(list.size() - 1)));
		for (int i = 0; i < list.size() - 1; i++) {
			sb.append(String.valueOf(list.get(i)));
		}
		msg = sb.toString();
		return msg;
	}

}
