package main;

import encoding.ExtendedGenerationMatrix;
import encoding.GenerationMatrix;

public class RMStart {
	
	//console fun
	//2.0 version

	public static void main(String[] args) {
		//message to be encoded
		String input = "0110";
		

		//RM code's parameters
		int r = 2;
		int m = 3;

		System.out.println("Encoding with Reed-Muller code");
		System.out.println("Message: "+input);
		System.out.println("Generation matrix for R("+r+","+m+") code");
		
		System.out.println("");
		System.out.println("");
		System.out.println("");
		System.out.println("");
		System.out.println("");
		System.out.println("Encoding for old matrix");

		GenerationMatrix mat2 = new GenerationMatrix(r, m);
		mat2.encodeMessage(input);
		System.out.println("");
		System.out.println("");
		System.out.println("");
		System.out.println("");
		System.out.println("");
		System.out.println("Encoding for new, extended matrix");
		
		ExtendedGenerationMatrix mat = new ExtendedGenerationMatrix(r, m);
		mat.encodeMessage(input);

	}

}
