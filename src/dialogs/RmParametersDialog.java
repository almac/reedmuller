package dialogs;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;

import javax.swing.JFormattedTextField;

import main.RMFrame;

public class RmParametersDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();

	private JFormattedTextField rText;
	private JFormattedTextField mText;

	/**
	 * Create the dialog.
	 */
	public RmParametersDialog(int r, int m) {

		NumberFormat numberFormat = NumberFormat.getNumberInstance();
		numberFormat.setMaximumIntegerDigits(1);

		setBounds(100, 100, 329, 179);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		GridBagLayout gbl_contentPanel = new GridBagLayout();
		gbl_contentPanel.columnWidths = new int[] { 123, 0, 0 };
		gbl_contentPanel.rowHeights = new int[] { 34, 0, 0, 0 };
		gbl_contentPanel.columnWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		gbl_contentPanel.rowWeights = new double[] { 0.0, 0.0, 0.0, Double.MIN_VALUE };
		contentPanel.setLayout(gbl_contentPanel);
		{
			JLabel dialogLabel = new JLabel("RM code parameters:");
			GridBagConstraints gbc_dialogLabel = new GridBagConstraints();
			gbc_dialogLabel.gridwidth = 2;
			gbc_dialogLabel.insets = new Insets(0, 0, 5, 0);
			gbc_dialogLabel.gridx = 0;
			gbc_dialogLabel.gridy = 0;
			contentPanel.add(dialogLabel, gbc_dialogLabel);
		}
		{
			JLabel rLabel = new JLabel("Parameter r:");
			GridBagConstraints gbc_rLabel = new GridBagConstraints();
			gbc_rLabel.insets = new Insets(0, 0, 5, 5);
			gbc_rLabel.gridx = 0;
			gbc_rLabel.gridy = 1;
			contentPanel.add(rLabel, gbc_rLabel);
		}
		{
			rText = new JFormattedTextField(numberFormat);
			rText.setText(Integer.toString(r));
			GridBagConstraints gbc_rText = new GridBagConstraints();
			gbc_rText.insets = new Insets(0, 0, 5, 0);
			gbc_rText.fill = GridBagConstraints.HORIZONTAL;
			gbc_rText.gridx = 1;
			gbc_rText.gridy = 1;
			contentPanel.add(rText, gbc_rText);
		}
		{
			JLabel mParameter = new JLabel("Parameter m:");
			GridBagConstraints gbc_mParameter = new GridBagConstraints();
			gbc_mParameter.insets = new Insets(0, 0, 0, 5);
			gbc_mParameter.gridx = 0;
			gbc_mParameter.gridy = 2;
			contentPanel.add(mParameter, gbc_mParameter);
		}
		{
			mText = new JFormattedTextField(numberFormat);
			mText.setText(Integer.toString(m));
			GridBagConstraints gbc_mText = new GridBagConstraints();
			gbc_mText.fill = GridBagConstraints.HORIZONTAL;
			gbc_mText.gridx = 1;
			gbc_mText.gridy = 2;
			contentPanel.add(mText, gbc_mText);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent arg0) {
						buttonOkAction();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
				cancelButton.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent arg0) {
						RmParametersDialog.this.dispose();
					}
				});
			}
		}
	}

	private void buttonOkAction() {

		String r = rText.getText();
		String m = mText.getText();

		if (r != null && m != null) {
			try {
				int rr = Integer.parseInt(r);
				RMFrame.rValue = rr;
				RMFrame.mValue = Integer.parseInt(m);

				if (rr >= 3) {
					JOptionPane.showMessageDialog(this, "Parameter r could be max 2!", "Error",
							JOptionPane.ERROR_MESSAGE);
				} else {
					this.dispose();
				}

			} catch (NumberFormatException e) {
				JOptionPane.showMessageDialog(this, "Wrong data format!", "Error", JOptionPane.ERROR_MESSAGE);
			}
		}

	}
}
