package encoding;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import main.RMFrame;

import utils.StringUtils;

public class ExtendedGenerationMatrix {

	/**
	 * Number of rows in matrix
	 */
	int k;
	/**
	 * Number of columns in matrix
	 */
	int c;
	/**
	 * the m value in R-M code R(r,m)
	 */
	int m;
	/**
	 * the r value in R-M code R(r,m)
	 */
	int r;

	/**
	 * Mapped matrix, Set is vectors which was taken to create rows in matrix,
	 * String vector in matrix
	 */
	Map<Set<Integer>, String> matrix;

	/**
	 * Generation matrix constructor
	 * 
	 * @param input_r
	 *            the r value in R-M code R(r,m)
	 * @param input_m
	 *            the m value in R-M code R(r,m)
	 */
	public ExtendedGenerationMatrix(int input_r, int input_m) {
		r = input_r;
		m = input_m;
		k = rowSize(r, m);
		c = columnSize(m);
		matrix = generateMatrix(r, m, c);
	}

	/**
	 * Elaborates number of columns in generation matrix
	 * 
	 * @param m
	 *            the m value in R-M code R(r,m)
	 * @return number of columns in matrix
	 */
	private int columnSize(int m) {
		int columns = (int) Math.pow(2, m);
		System.out.println("Columns: " + columns);
		return columns;
	}

	/**
	 * Elaborates number of rows in generation matrix
	 * 
	 * @param r
	 *            the r value in R-M code R(r,m)
	 * @param m
	 *            the m value in R-M code R(r,m)
	 * @return number of rows in matrix
	 */
	private int rowSize(int r, int m) {
		int rows = 1;

		for (int i = 1; i <= r; i++) {
			rows += newton(m, i);
		}
		System.out.println("Rows: " + rows);
		return rows;
	}

	/**
	 * Calculates value of Newton coefficient
	 * 
	 * @param n
	 *            the n value
	 * @param k
	 *            the k value
	 * @return value of newton coefficient
	 */
	private long newton(int n, int k) {
		double newton = 1;
		for (int i = 1; i <= k; i++) {
			newton = newton * (n - i + 1) / i;
		}
		return (long) newton;
	}

	/**
	 * Generates generation matix
	 * 
	 * @param r
	 *            the r value in R-M code R(r,m)
	 * @param m
	 *            the m value in R-M code R(r,m)
	 * @param c
	 *            number of generation matrix columns
	 * @return
	 */
	public Map<Set<Integer>, String> generateMatrix(int r, int m, int c) {

		Map<Set<Integer>, String> matrix = new LinkedHashMap<Set<Integer>, String>();
		Set<Integer> indexSet;
		if (r == 0) {
			String first = StringUtils.generateOnes(m);
			indexSet = new HashSet<Integer>();
			indexSet.add(r);
			matrix.put(indexSet, first);
		} else {
			String first = StringUtils.generateOnes(m);
			indexSet = new HashSet<Integer>();
			indexSet.add(0);
			matrix.put(indexSet, first);

			for (int i = 1; i <= m; i++) {
				indexSet = new HashSet<Integer>();
				indexSet.add(i);
				String ones = StringUtils.generateOnes(m - i);
				String zeros = StringUtils.generateZeros(m - i);
				String batch = zeros + ones;
				int extensions = c / batch.length();
				StringBuffer sb1 = new StringBuffer();
				for (int j = 0; j < extensions; j++) {
					sb1.append(batch);
				}
				matrix.put(indexSet, sb1.toString());
			}

			if (r >= 2) {
				int z = 2;
				for (; z <= m; z++) {
					for (int y = 1; y < z; y++) {

						indexSet = new HashSet<Integer>();

						indexSet.add(z);
						indexSet.add(y);
						long a = Long.parseLong(matrix.get(new HashSet<Integer>(Arrays.asList(z))), 2);
						long b = Long.parseLong(matrix.get(new HashSet<Integer>(Arrays.asList(y))), 2);
						long cc = a & b;

						String end = StringUtils.toBinaryString(cc, c);
						matrix.put(indexSet, end);
					}
				}
			}

		}
		printMatrix(matrix);
		return matrix;
	}

	/**
	 * Encodes message
	 * 
	 * @param message
	 *            message to encode
	 * @return encoded message
	 */
	public String encodeMessage(String message) {
		if (message.length() > k) {
			return "Message is too long for this RM code?";
		}
		String inputs = StringUtils.padLeft(message, k);
		System.out.println("Message to encode:");
		System.out.println(inputs);

		ArrayList<String> encodedMatrix = new ArrayList<String>();
		long encodedMessage = -1;

		long onesVector = StringUtils.toBinaryValue(StringUtils.generateOnes(m));
		long zeroVector = StringUtils.toBinaryValue(StringUtils.generateZeros(m));

		ArrayList<Long> valueList = getValuesFromMatrix();

		for (int i = 0; i < inputs.length(); i++) {
			long matrixRow = valueList.get(i);
			switch (inputs.charAt(i)) {
				case '1':
					long encoded1 = matrixRow & onesVector;
					encodedMatrix.add(StringUtils.toBinaryString(encoded1, c));
					break;
				case '0':
					long encoded0 = matrixRow & zeroVector;
					encodedMatrix.add(StringUtils.toBinaryString(encoded0, c));
					break;
			}
		}
		System.out.println("Encoded matrix:");
		printMatrix(encodedMatrix);

		for (String s : encodedMatrix) {
			long value = Integer.parseInt(s, 2);
			if (encodedMessage == -1) {
				encodedMessage = value;
			} else {
				encodedMessage ^= value;
			}
		}
		String enc = StringUtils.toBinaryString(encodedMessage, c);
		System.out.println("Encoded message: ");
		System.out.println(enc);
		return enc;

	}

	/**
	 * Gets decimal values from rows in generation matrix
	 * 
	 * @return list of binary values of matrix's rows
	 */
	public ArrayList<Long> getValuesFromMatrix() {
		ArrayList<Long> list = new ArrayList<Long>();
		for (Map.Entry<Set<Integer>, String> e : matrix.entrySet()) {
			list.add(StringUtils.toBinaryValue(e.getValue()));
		}
		return list;
	}

	/**
	 * Prints matrix in console
	 * 
	 * @param matrix
	 *            matrix to print
	 */
	public void printMatrix(Map<Set<Integer>, String> matrix) {
		for (Map.Entry<Set<Integer>, String> e : matrix.entrySet())
			System.out.println(e.getKey() + ": " + e.getValue());
	}

	/**
	 * Prints matrix in console
	 * 
	 * @param matrix
	 *            matrix to print
	 */
	public void printMatrix(ArrayList<String> matrix) {
		for (String e : matrix)
			System.out.println(e);
	}

	/**
	 * Get number of rows
	 * 
	 * @return number of rows
	 */
	public int getK() {
		return k;
	}

	/**
	 * Get number of columns
	 * 
	 * @return number of columns
	 */
	public int getC() {
		return c;
	}

	/**
	 * Get the m value in R-M code R(r,m)
	 * 
	 * @return the m value in R-M code R(r,m)
	 */
	public int getM() {
		return m;
	}

	/**
	 * Get the r value in R-M code R(r,m)
	 * 
	 * @return the r value in R-M code R(r,m)
	 */
	public int getR() {
		return r;
	}

	/**
	 * Get generation matrix
	 * 
	 * @return generation matrix
	 */
	public Map<Set<Integer>, String> getMatrix() {
		return matrix;
	}

	/**
	 * Calculates number of characteristic vectors
	 * 
	 * @return number of characteristic vectors
	 */
	public int getCharacteristicVectorsNumber() {
		return (int) Math.pow(2, (getM() - getR()));
	}

	/**
	 * Creates collection of characteristic vectors
	 * 
	 * @return collection of characteristic vectors
	 */
	public Map<Set<Integer>, Set<Integer>> getCharacteristicVectors() {
		// TODO for now it only prints vectors that aren't dependend
		Map<Set<Integer>, Set<Integer>> characteristicVectors = new LinkedHashMap<Set<Integer>, Set<Integer>>();

		matrix.remove(new HashSet<Integer>(Arrays.asList(0)));

		for (Map.Entry<Set<Integer>, String> entry : matrix.entrySet()) {

			Set<Integer> characteristicVectorsSet = new HashSet<Integer>();
			for (Map.Entry<Set<Integer>, String> possibleCharacteristicVector : matrix.entrySet()) {
				if (possibleCharacteristicVector.getKey().size() == 1
						&& !entry.getKey().containsAll(possibleCharacteristicVector.getKey())) {
					characteristicVectorsSet.add(possibleCharacteristicVector.getKey().iterator().next());
				}
			}
			characteristicVectors.put(entry.getKey(), characteristicVectorsSet);

		}

		return characteristicVectors;
	}

	public static int getMaxErrors() {
		return (int) Math.pow(2, RMFrame.mValue - RMFrame.rValue - 1) - 1;
	}
}
