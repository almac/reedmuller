package encoding;

import java.util.ArrayList;

import utils.StringUtils;

public class GenerationMatrix { 

	/**
	 * Number of rows in matrix
	 */
	int k;
	/**
	 * Number of columns in matrix
	 */
	int c;
	/**
	 * the m value in R-M code R(r,m)
	 */
	int m;
	/**
	 * the r value in R-M code R(r,m)
	 */
	int r;
	ArrayList<String> matrix;

	/**
	 * Generation matrix constructor
	 * 
	 * @param input_r
	 *            the r value in R-M code R(r,m)
	 * @param input_m
	 *            the m value in R-M code R(r,m)
	 */
	public GenerationMatrix(int input_r, int input_m) {
		r = input_r;
		m = input_m;
		k = rowSize(r, m);
		c = columnSize(m);
		matrix = generateMatrix(r, m, c);
	}

	/**
	 * Elaborates number of columns in generation matrix
	 * 
	 * @param m
	 *            the m value in R-M code R(r,m)
	 * @return number of columns in matrix
	 */
	private int columnSize(int m) {
		int columns = (int) Math.pow(2, m);
		System.out.println("Columns: " + columns);
		return columns;
	}

	/**
	 * Elaborates number of rows in generation matrix
	 * 
	 * @param r
	 *            the r value in R-M code R(r,m)
	 * @param m
	 *            the m value in R-M code R(r,m)
	 * @return number of rows in matrix
	 */
	private int rowSize(int r, int m) {
		int rows = 1;

		for (int i = 1; i <= r; i++) {
			rows += newton(m, i);
		}
		System.out.println("Rows: " + rows);
		return rows;
	}

	/**
	 * Calculates value of Newton coefficient
	 * 
	 * @param n
	 *            the n value
	 * @param k
	 *            the k value
	 * @return value of newton coefficient
	 */
	private long newton(int n, int k) {
		double newton = 1;
		for (int i = 1; i <= k; i++) {
			newton = newton * (n - i + 1) / i;
		}
		return (long) newton;
	}

	/**
	 * Generates generation matix
	 * 
	 * @param r
	 *            the r value in R-M code R(r,m)
	 * @param m
	 *            the m value in R-M code R(r,m)
	 * @param c
	 *            number of generation matrix columns
	 * @return
	 */
	public ArrayList<String> generateMatrix(int r, int m, int c) {

		ArrayList<String> matrix = new ArrayList<String>();
		if (r == 0) {
			String first = StringUtils.generateOnes(m);
			matrix.add(first);
		} else {
			String first = StringUtils.generateOnes(m);
			matrix.add(first);

			for (int i = 1; i <= m; i++) {

				String ones = StringUtils.generateOnes(m - i);
				String zeros = StringUtils.generateZeros(m - i);
				String batch = zeros + ones;
				int extensions = c / batch.length();
				StringBuffer sb1 = new StringBuffer();
				for (int j = 0; j < extensions; j++) {
					sb1.append(batch);
				}
				matrix.add(sb1.toString());
			}

			if (r >= 2) {
				int z = 2;
				for (; z <= m; z++) {
					for (int y = 1; y < z; y++) {
//
//						System.out.println("----------");
//						System.out.println("z="+z+" y="+y);
//						System.out.println("----------");
						
						long a = Long.parseLong(matrix.get(z), 2);
						long b = Long.parseLong(matrix.get(y), 2);
						long cc = a & b;

						String end = StringUtils.toBinaryString(cc, c);
						matrix.add(end);
					}
				}
			}

		}
		printMatrix(matrix);
		return matrix;
	}

	/**
	 * Encodes message
	 * 
	 * @param message
	 *            message to encode
	 * @return encoded message
	 */
	public String encodeMessage(String message) {
		if(message.length()>k){
			return "Message is too long for this RM code?";
		}
		String inputs = StringUtils.padLeft(message, k);
		System.out.println("Message to encode:");
		System.out.println(inputs);

		ArrayList<String> encodedMatrix = new ArrayList<String>();
		long encodedMessage = -1;

		long onesVector = StringUtils.toBinaryValue(StringUtils.generateOnes(m));
		long zeroVector = StringUtils.toBinaryValue(StringUtils.generateZeros(m));

		for (int i = 0; i < inputs.length(); i++) {
			long matrixRow = Long.parseLong(matrix.get(i), 2);
			switch (inputs.charAt(i)) {
				case '1':
					long encoded1 = matrixRow & onesVector;
					encodedMatrix.add(StringUtils.toBinaryString(encoded1, c));
					break;
				case '0':
					long encoded0 = matrixRow & zeroVector;
					encodedMatrix.add(StringUtils.toBinaryString(encoded0, c));
					break;
			}
		}
		System.out.println("Encoded matrix:");
		printMatrix(encodedMatrix);

		for (String s : encodedMatrix) {
			long value = Integer.parseInt(s, 2);
			if (encodedMessage == -1) {
				encodedMessage = value;
			} else {
				encodedMessage ^= value;
			}
		}
		String enc = StringUtils.toBinaryString(encodedMessage, c);
		System.out.println("Encoded message: ");
		System.out.println(enc);
		return enc;

	}

	/**
	 * Prints matrix in console
	 * 
	 * @param matrix
	 *            matrix to print
	 */
	private void printMatrix(ArrayList<String> matrix) {
		for (String s : matrix) {
			System.out.println(s);
		}
	}

	/**
	 * Get number of rows
	 * 
	 * @return number of rows
	 */
	public int getK() {
		return k;
	}

	/**
	 * Get number of columns
	 * 
	 * @return number of columns
	 */
	public int getC() {
		return c;
	}

	/**
	 * Get the m value in R-M code R(r,m)
	 * 
	 * @return the m value in R-M code R(r,m)
	 */
	public int getM() {
		return m;
	}

	/**
	 * Get the r value in R-M code R(r,m)
	 * 
	 * @return the r value in R-M code R(r,m)
	 */
	public int getR() {
		return r;
	}

	/**
	 * Get generation matrix
	 * 
	 * @return generation matrix
	 */
	public ArrayList<String> getMatrix() {
		return matrix;
	}

}
