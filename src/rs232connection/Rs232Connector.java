package rs232connection;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.nio.ByteBuffer;

import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;

public class Rs232Connector {

	static String response = "";
	static PropertyChangeSupport propertyChangeSupport;

	public Rs232Connector(String port) {
		propertyChangeSupport = new PropertyChangeSupport(this);
		try {
			connect(port);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		propertyChangeSupport.addPropertyChangeListener(listener);
	}

	public static void setResponse(String text) {
		String oldText = response;
		response = text;
		propertyChangeSupport.firePropertyChange("MyTextProperty", oldText, text);
	}

	private void connect(String portName) throws Exception {
		CommPortIdentifier portIdentifier = CommPortIdentifier.getPortIdentifier(portName);

		System.out.println(portIdentifier.getName());
		if (portIdentifier.isCurrentlyOwned()) {
			System.out.println("Port in use!");
		} else {
			// points who owns the port and connection timeout
			SerialPort serialPort = (SerialPort) portIdentifier.open("Rs232Connector", 2000);

			// setup connection parameters
			serialPort.setSerialPortParams(9600, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);

			// setup serial port writer
			CommPortSender.setWriterStream(serialPort.getOutputStream());

			// setup serial port reader
			new CommPortReceiver(serialPort.getInputStream()).start();
		}
	}

	public void sendMessage(String message) {
//		 String preparedMessage = prepareMessageToByte(message);
		 int charCode = Integer.parseInt(message, 2);
		 byte[] bytes = new byte[]{(byte) charCode};
//		 CommPortSender.send(new ProtocolImpl().getMessage(preparedMessage));
		 CommPortSender.send(bytes);
	}

//	private String prepareMessageToByte(String message) {
//		int charCode = Integer.parseInt(message, 2);
//		String preparedMessage = Integer.toString(charCode);
//		return preparedMessage;
//	}

}
