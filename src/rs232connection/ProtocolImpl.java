package rs232connection;

public class ProtocolImpl implements Protocol {

	byte[] buffer = new byte[1024];
	int tail = 0;

	public void onReceive(byte b) {
		onMessage(b);
	}

	public void onStreamClosed() {
	}

	/*
	 * When message is recognized onMessage is invoked
	 */
	private void onMessage(byte b) {
		String message = String.format("%8s", Integer.toBinaryString(b & 0xFF)).replace(' ', '0');
		System.out.println("RECEIVED MESSAGE: " + message);
		Rs232Connector.setResponse(message);
	}

}