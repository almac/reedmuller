package rs232connection;

import java.io.IOException;  
import java.io.OutputStream;  
   
public class CommPortSender {  
   
    static OutputStream out;  
      
    public static void setWriterStream(OutputStream out) {  
        CommPortSender.out = out;  
    }  
      
    public static void send(byte[] bytes) {  
        try {
        	for(byte b: bytes){
        		String s1 = String.format("%8s", Integer.toBinaryString(b & 0xFF)).replace(' ', '0');
        		System.out.println("SENDING: " +s1);
        	} 
              
            // sending through serial port is simply writing into OutputStream  
            out.write(bytes);  
            out.flush();  
        } catch (IOException e) {  
            e.printStackTrace();  
        }  
    }  
}  