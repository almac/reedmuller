package panels;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTextPane;

import encoding.GenerationMatrix;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;

import main.RMFrame;

public class EncodingPanel extends JPanel {

	/**
	 * Text field to write message to encode
	 */
	private JTextField messageTextField;
	/**
	 * Text pane in which encoded message will be shown
	 */
	private JTextPane encodedMessageTextPane;
	/**
	 * Generarion matrix used to encode messages
	 */
	private GenerationMatrix matrix;
	/**
	 * Encoding button
	 */
	private JButton encodeButton;
	/**
	 * Button which shows generation matrix
	 */
	private JButton genMatrixButton;

	public EncodingPanel() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, 0.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		setLayout(gridBagLayout);

		JPanel encodingPanel = new JPanel();
		GridBagConstraints gbc_encodingPanel = new GridBagConstraints();
		gbc_encodingPanel.insets = new Insets(0, 0, 0, 5);
		gbc_encodingPanel.fill = GridBagConstraints.BOTH;
		gbc_encodingPanel.gridx = 0;
		gbc_encodingPanel.gridy = 0;
		add(encodingPanel, gbc_encodingPanel);
		GridBagLayout gbl_encodingPanel = new GridBagLayout();
		gbl_encodingPanel.columnWidths = new int[] { 0, 0, 0, 0 };
		gbl_encodingPanel.rowHeights = new int[] { 0, 0, 0, 196, 9, 0 };
		gbl_encodingPanel.columnWeights = new double[] { 0.0, 1.0, 0.0, Double.MIN_VALUE };
		gbl_encodingPanel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		encodingPanel.setLayout(gbl_encodingPanel);

		JLabel messageLabel = new JLabel("Message");
		GridBagConstraints gbc_messageLabel = new GridBagConstraints();
		gbc_messageLabel.insets = new Insets(0, 0, 5, 5);
		gbc_messageLabel.gridx = 1;
		gbc_messageLabel.gridy = 0;
		encodingPanel.add(messageLabel, gbc_messageLabel);

		messageTextField = new JTextField();
		GridBagConstraints gbc_messageTextField = new GridBagConstraints();
		gbc_messageTextField.insets = new Insets(0, 0, 5, 5);
		gbc_messageTextField.fill = GridBagConstraints.HORIZONTAL;
		gbc_messageTextField.gridx = 1;
		gbc_messageTextField.gridy = 1;
		encodingPanel.add(messageTextField, gbc_messageTextField);
		messageTextField.setColumns(10);

		JLabel encodedMessageLabel = new JLabel("Encoded message");
		GridBagConstraints gbc_encodedMessageLabel = new GridBagConstraints();
		gbc_encodedMessageLabel.insets = new Insets(0, 0, 5, 5);
		gbc_encodedMessageLabel.gridx = 1;
		gbc_encodedMessageLabel.gridy = 2;
		encodingPanel.add(encodedMessageLabel, gbc_encodedMessageLabel);

		encodedMessageTextPane = new JTextPane();
		GridBagConstraints gbc_encodedMessageTextPane = new GridBagConstraints();
		gbc_encodedMessageTextPane.insets = new Insets(0, 0, 5, 5);
		gbc_encodedMessageTextPane.fill = GridBagConstraints.BOTH;
		gbc_encodedMessageTextPane.gridx = 1;
		gbc_encodedMessageTextPane.gridy = 3;
		encodingPanel.add(encodedMessageTextPane, gbc_encodedMessageTextPane);

		JPanel buttonPanel = new JPanel();
		GridBagConstraints gbc_buttonPanel = new GridBagConstraints();
		gbc_buttonPanel.fill = GridBagConstraints.BOTH;
		gbc_buttonPanel.gridx = 1;
		gbc_buttonPanel.gridy = 0;
		add(buttonPanel, gbc_buttonPanel);
		GridBagLayout gbl_buttonPanel = new GridBagLayout();
		gbl_buttonPanel.columnWidths = new int[] { 37, 148, 34, 0 };
		gbl_buttonPanel.rowHeights = new int[] { 0, 0, 0 };
		gbl_buttonPanel.columnWeights = new double[] { 0.0, 0.0, 0.0, Double.MIN_VALUE };
		gbl_buttonPanel.rowWeights = new double[] { 0.0, 0.0, Double.MIN_VALUE };
		buttonPanel.setLayout(gbl_buttonPanel);

		encodeButton = new JButton("Encode message");
		GridBagConstraints gbc_encodeButton = new GridBagConstraints();
		gbc_encodeButton.insets = new Insets(0, 0, 5, 5);
		gbc_encodeButton.gridx = 1;
		gbc_encodeButton.gridy = 0;
		encodeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				encodeMessage();
			}
		});
		buttonPanel.add(encodeButton, gbc_encodeButton);

		genMatrixButton = new JButton("Show generation matrix");
		GridBagConstraints gbc_genMatrixButton = new GridBagConstraints();
		gbc_genMatrixButton.insets = new Insets(0, 0, 0, 5);
		gbc_genMatrixButton.gridx = 1;
		gbc_genMatrixButton.gridy = 1;
		genMatrixButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				showGenerationMatrix(matrix);
			}
		});
		genMatrixButton.setEnabled(false);
		buttonPanel.add(genMatrixButton, gbc_genMatrixButton);

	}

	/**
	 * Creates dialog window in wich generation matrix is shown
	 * 
	 * @param matrix
	 *            matrix to be shown
	 */
	private void showGenerationMatrix(GenerationMatrix matrix) {
		String matrixRows;
		StringBuffer sb = new StringBuffer();

		for (String s : matrix.getMatrix()) {
			sb.append(s).append(" \n");
		}
		matrixRows = sb.toString();

		JOptionPane.showMessageDialog(this,
				"Generation matrix for " + "Reed-Muller R(" + matrix.getR() + "," + matrix.getM() + ") code:" + "\n"
						+ matrixRows, "Generation matrix", JOptionPane.INFORMATION_MESSAGE);
	}

	/**
	 * Encodes message
	 */
	private void encodeMessage() {
		String message = messageTextField.getText();
		if (message.length() != 0) {
			matrix = new GenerationMatrix(RMFrame.rValue, RMFrame.mValue);
			String encodedMessage = matrix.encodeMessage(message);
			encodedMessageTextPane.setText(encodedMessage);
			genMatrixButton.setEnabled(true);
		}
	}

}
