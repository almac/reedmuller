package panels;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTextPane;

import main.RMFrame;
import rs232connection.Rs232Connector;
import utils.StringUtils;
import utils.VectorUtils;
import decoding.Decoder;
import encoding.ExtendedGenerationMatrix;
import javax.swing.JCheckBox;

public class DecodingPanel extends JPanel {

	/**
	 * Text field to write encoded message to encode
	 */
	private JTextField messageTextField;
	/**
	 * Text pane in which decoded message will be shown
	 */
	private JTextPane decodedMessageTextPane;
	/**
	 * Generarion matrix used to decode messages
	 */
	private HashMap<Set<Integer>, Integer> vectorNumberToMajorityMap;
	/**
	 * Message to decode
	 */
	private String message;

	/**
	 * Bits of correct message
	 */
	private ArrayList<Integer> correctMessage;
	/**
	 * Decoding button
	 */
	private JButton decodeButton;

	/**
	 * Message with random errors
	 */
	private JLabel messageWithErrors;

	private int charactristicVectorsNumber;

	final JCheckBox chckbxAddRandomErrors;
	final JCheckBox chckbxSendToFpga;
	JTextPane decodedFPGAMessageTextPane;
	Rs232Connector connector;

	/**
	 * Create the panel.
	 */
	public DecodingPanel() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, 0.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		setLayout(gridBagLayout);

		JPanel encodingPanel = new JPanel();
		GridBagConstraints gbc_encodingPanel = new GridBagConstraints();
		gbc_encodingPanel.insets = new Insets(0, 0, 0, 5);
		gbc_encodingPanel.fill = GridBagConstraints.BOTH;
		gbc_encodingPanel.gridx = 0;
		gbc_encodingPanel.gridy = 0;
		add(encodingPanel, gbc_encodingPanel);
		GridBagLayout gbl_encodingPanel = new GridBagLayout();
		gbl_encodingPanel.columnWidths = new int[] { 0, 0, 0, 0 };
		gbl_encodingPanel.rowHeights = new int[] { 0, 0, 0, 0, 0, 80, 24, 80, 0 };
		gbl_encodingPanel.columnWeights = new double[] { 0.0, 1.0, 0.0, Double.MIN_VALUE };
		gbl_encodingPanel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		encodingPanel.setLayout(gbl_encodingPanel);

		JLabel encodedMessageLabel = new JLabel("Encoded message");
		GridBagConstraints gbc_encodedMessageLabel = new GridBagConstraints();
		gbc_encodedMessageLabel.insets = new Insets(0, 0, 5, 5);
		gbc_encodedMessageLabel.gridx = 1;
		gbc_encodedMessageLabel.gridy = 0;
		encodingPanel.add(encodedMessageLabel, gbc_encodedMessageLabel);

		messageTextField = new JTextField();
		GridBagConstraints gbc_messageTextField = new GridBagConstraints();
		gbc_messageTextField.insets = new Insets(0, 0, 5, 5);
		gbc_messageTextField.fill = GridBagConstraints.HORIZONTAL;
		gbc_messageTextField.gridx = 1;
		gbc_messageTextField.gridy = 1;
		encodingPanel.add(messageTextField, gbc_messageTextField);
		messageTextField.setColumns(10);

		JLabel messageWithErrorsLabel = new JLabel("Message with errors");
		GridBagConstraints gbc_messageWithErrorsLabel = new GridBagConstraints();
		gbc_messageWithErrorsLabel.insets = new Insets(0, 0, 5, 5);
		gbc_messageWithErrorsLabel.gridx = 1;
		gbc_messageWithErrorsLabel.gridy = 2;
		encodingPanel.add(messageWithErrorsLabel, gbc_messageWithErrorsLabel);

		messageWithErrors = new JLabel(" ");
		GridBagConstraints gbc_messageWithErrors = new GridBagConstraints();
		gbc_messageWithErrors.insets = new Insets(0, 0, 5, 5);
		gbc_messageWithErrors.gridx = 1;
		gbc_messageWithErrors.gridy = 3;
		encodingPanel.add(messageWithErrors, gbc_messageWithErrors);

		JLabel decodedMessageLabel = new JLabel("Decoded message");
		GridBagConstraints gbc_decodedMessageLabel = new GridBagConstraints();
		gbc_decodedMessageLabel.insets = new Insets(0, 0, 5, 5);
		gbc_decodedMessageLabel.gridx = 1;
		gbc_decodedMessageLabel.gridy = 4;
		encodingPanel.add(decodedMessageLabel, gbc_decodedMessageLabel);

		decodedMessageTextPane = new JTextPane();
		GridBagConstraints gbc_decodedMessageTextPane = new GridBagConstraints();
		gbc_decodedMessageTextPane.insets = new Insets(0, 0, 5, 5);
		gbc_decodedMessageTextPane.fill = GridBagConstraints.BOTH;
		gbc_decodedMessageTextPane.gridx = 1;
		gbc_decodedMessageTextPane.gridy = 5;
		encodingPanel.add(decodedMessageTextPane, gbc_decodedMessageTextPane);

		JLabel decodedFPGAMessageLabel = new JLabel("Decoded message from FPGA");
		GridBagConstraints gbc_decodedFPGAMessageLabel = new GridBagConstraints();
		gbc_decodedFPGAMessageLabel.insets = new Insets(0, 0, 5, 5);
		gbc_decodedFPGAMessageLabel.gridx = 1;
		gbc_decodedFPGAMessageLabel.gridy = 6;
		encodingPanel.add(decodedFPGAMessageLabel, gbc_decodedFPGAMessageLabel);

		decodedFPGAMessageTextPane = new JTextPane();
		GridBagConstraints gbc_decodedFPGAMessageTextPane = new GridBagConstraints();
		gbc_decodedFPGAMessageTextPane.insets = new Insets(0, 0, 0, 5);
		gbc_decodedFPGAMessageTextPane.fill = GridBagConstraints.BOTH;
		gbc_decodedFPGAMessageTextPane.gridx = 1;
		gbc_decodedFPGAMessageTextPane.gridy = 7;
		encodingPanel.add(decodedFPGAMessageTextPane, gbc_decodedFPGAMessageTextPane);

		JPanel buttonPanel = new JPanel();
		GridBagConstraints gbc_buttonPanel = new GridBagConstraints();
		gbc_buttonPanel.fill = GridBagConstraints.BOTH;
		gbc_buttonPanel.gridx = 1;
		gbc_buttonPanel.gridy = 0;
		add(buttonPanel, gbc_buttonPanel);
		GridBagLayout gbl_buttonPanel = new GridBagLayout();
		gbl_buttonPanel.columnWidths = new int[] { 37, 148, 34, 0 };
		gbl_buttonPanel.rowHeights = new int[] { 0, 0, 0, 0 };
		gbl_buttonPanel.columnWeights = new double[] { 0.0, 0.0, 0.0, Double.MIN_VALUE };
		gbl_buttonPanel.rowWeights = new double[] { 0.0, 0.0, 0.0, Double.MIN_VALUE };
		buttonPanel.setLayout(gbl_buttonPanel);

		decodeButton = new JButton("Decode message");
		GridBagConstraints gbc_decodeButton = new GridBagConstraints();
		gbc_decodeButton.insets = new Insets(0, 0, 5, 5);
		gbc_decodeButton.gridx = 1;
		gbc_decodeButton.gridy = 0;
		decodeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				decodeActionButton();

			}
		});
		buttonPanel.add(decodeButton, gbc_decodeButton);

		chckbxAddRandomErrors = new JCheckBox("Add random errors");
		chckbxAddRandomErrors.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (chckbxAddRandomErrors.isSelected()) {
				} else {
					messageWithErrors.setText("");
				}
			}
		});
		GridBagConstraints gbc_chckbxAddRandomErrors = new GridBagConstraints();
		gbc_chckbxAddRandomErrors.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxAddRandomErrors.gridx = 1;
		gbc_chckbxAddRandomErrors.gridy = 1;
		buttonPanel.add(chckbxAddRandomErrors, gbc_chckbxAddRandomErrors);

		chckbxSendToFpga = new JCheckBox("Send to FPGA Decoder");
		GridBagConstraints gbc_chckbxSendToFpga = new GridBagConstraints();
		gbc_chckbxSendToFpga.insets = new Insets(0, 0, 0, 5);
		gbc_chckbxSendToFpga.gridx = 1;
		gbc_chckbxSendToFpga.gridy = 2;
		buttonPanel.add(chckbxSendToFpga, gbc_chckbxSendToFpga);

	}

	private int prepareMessageToDecode() {

		message = messageTextField.getText();

		vectorNumberToMajorityMap = new HashMap<Set<Integer>, Integer>();
		correctMessage = new ArrayList<Integer>();
		int messageCorrectLength = (int) Math.pow(2, RMFrame.mValue);
		if (message.length() != messageCorrectLength) {
			JOptionPane.showMessageDialog(this, "Decoded message must have " + messageCorrectLength
					+ " characters, not " + message.length() + ".", "Error", JOptionPane.ERROR_MESSAGE);
			return 0;
		}

		message = getMessageToEncode();
		return 1;
	}

	private String getMessageToEncode() {
		String encode = message;
		if (chckbxAddRandomErrors.isSelected()) {
			int maxErrors = ExtendedGenerationMatrix.getMaxErrors();
			int errors = (int) Math.random() * 50 % maxErrors + 1;
			ArrayList<Integer> errorPlace = new ArrayList<Integer>();
			System.out.println("Erros " + errors);
			for (int i = 1; i <= errors; ++i) {
				errorPlace.add((int) (Math.random() * 50 % message.length()));
			}
			System.out.println("OLD " + encode);
			for (Integer e : errorPlace) {
				System.out.println("Change place " + e);
				switch (encode.charAt(e)) {
					case '1':
						char[] newEncode = encode.toCharArray();
						newEncode[e] = '0';
						encode = String.valueOf(newEncode);
						break;
					case '0':
						char[] newEncode2 = encode.toCharArray();
						newEncode2[e] = '1';
						encode = String.valueOf(newEncode2);
						break;
				}
				System.out.println("New " + encode);
			}
			messageWithErrors.setText(encode);
		}
		return encode;
	}

	private void decodeActionButton() {
		if (prepareMessageToDecode() == 1) {

			Decoder decoder = new Decoder();

			if (decoder.decodeMessage(message) == -1) {
				JOptionPane.showMessageDialog(DecodingPanel.this, "Too much errors in message to decode!", "Error",
						JOptionPane.ERROR_MESSAGE);
			} else {
				String result = decoder.getDecodedMessage();
				decodedMessageTextPane.setText(result);
			}
			if (chckbxSendToFpga.isSelected()) {
				if (RMFrame.mValue == 3 && RMFrame.rValue == 1) {
					if (connector == null) {
						connector = new Rs232Connector("COM9");
						connector.addPropertyChangeListener(new PropertyChangeListener() {

							@Override
							public void propertyChange(PropertyChangeEvent evt) {
								String newValue = (String) evt.getNewValue();
								decodedFPGAMessageTextPane.setText(newValue);
							}
						});
					}

					connector.sendMessage(message);

				} else {
					decodedFPGAMessageTextPane.setText("Decoding on FPGA works only for RM(1,3) code");
				}
			}

		}
	}

}
